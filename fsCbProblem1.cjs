const fs = require("fs");
const path = require("path");

function newDirectory(pathOfDirectory, numberOfFiles) {
    return new Promise((resolve, reject) => {
        fs.mkdir(pathOfDirectory, (err) => {
            if (err) {
                reject(err);
            }
            else {
                resolve([pathOfDirectory, numberOfFiles])
            }
        })
    })
}

function newData(newFile) {
    let data = {
        someData: newFile,
    }
    return JSON.stringify(data);
}

function createFiles(pathOfDirectory, numberOfFiles) {
    return new Promise((resolve, reject) => {
        let files = 0;
        let filesArray = [];
        for (let index = 1; index <= numberOfFiles; index++) {
            if (files < numberOfFiles) {
                let newFile = `file${index}.json`;
                const filePath = path.join(pathOfDirectory, newFile);
                let getData = newData(newFile);
                files++;
                filesArray.push(filePath)

                fs.writeFile(filePath, getData, (err) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        if (files === numberOfFiles) {
                            resolve([`Total of ${numberOfFiles} are created`, numberOfFiles, filesArray]);
                        }
                    }
                })

            }
        }
    })
}


function deleteFiles(numberOfFiles, array) {
    return new Promise((resolve, reject) => {
        for (let index = 0; index < numberOfFiles; index++) {
            let fileName = array[index];
            fs.unlink(fileName, (err) => {
                if (err) {
                    reject(err);
                }
                else {
                    if (index == array.length - 1) {
                        resolve('All files deleted');
                    }
                }
            })
        }
    })

}
function fsCbProblem1(pathOfDirectory, numberOfFiles) {
    newDirectory(pathOfDirectory, numberOfFiles)
        .then(([pathOfDirectory, numberOfFiles]) => {
            return createFiles(pathOfDirectory, numberOfFiles);
        })
        .then(([message, numberOfFiles, array]) => {
            console.log(message);
            return deleteFiles(numberOfFiles, array);
        })
        .then((message) => {
            console.log(message);
        })
        .catch((err) => {
            console.log(err);
        })
}



module.exports = fsCbProblem1;