
const fs = require('fs');
const path = require('path');

function readFile(file) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(data);
            }
        })
    })
}


function writeFile(newFile, data, filesArray) {
    return new Promise((resolve, reject) => {
        fs.writeFile(newFile, data, (err) => {
            if (err) {
                reject(err);
            }
            else {
                let newFileDir=path.join(__dirname,newFile)
                filesArray.push(newFileDir);
                resolve([data, filesArray]);
            }
        })
    })
}

function writeFilesFromArray(newFile, data, filesArray) {
    return new Promise((resolve, reject) => {
        fs.writeFile(newFile, data, (err) => {
            if (err) {
                reject(err);
            }
            else {
                resolve([data, filesArray])
            }
        })
    })
}

function toupper(data) {
    return new Promise((resolve, reject) => {
        let newData = data.toUpperCase();
        resolve(newData);
    })
}

function tolower(data, filesArray) {
    return new Promise((resolve, reject) => {
        let newData = data.toLowerCase();
        let splitData = newData.split(' ')
        resolve([splitData.join(' '), filesArray]);
    })
}

function sortFile(filesArray) {
    return new Promise((resolve, reject) => {
        let filesContent = filesArray.map((file) => {
            return fs.readFileSync(file, 'utf8')
        })
        let sortedFileContent = filesContent.sort()
        let joinedFileContent = sortedFileContent.join('\n');
        resolve([joinedFileContent, filesArray])
    })

}

function deleteFiles(filesDelete) {
    return new Promise((resolve, reject) => {
        filesDelete.map((file) => {
            fs.unlink(file, (err) => {
                if (err) {
                    reject(err);
                }
            })
        })
        resolve('All files deleted');
    })

}


function fsCbProblem2() {
    let filesArray = [];
    readFile('lipsum.txt')
        .then((data) => {
            return toupper(data);
        })
        .then((data) => {
            return writeFile('upperCase.txt', data, filesArray)
        })
        .then(([data, filesArray]) => {
            return tolower(data, filesArray);
        })
        .then(([data, filesArray]) => {
            return writeFile('lowerCase.txt', data, filesArray)
        })
        .then(([data, filesArray]) => {
            return sortFile(filesArray);
        })
        .then(([data, filesArray]) => {
            return writeFile('sorted.txt', data, filesArray);
        })
        .then(([data, filesArray]) => {
            let allFilesNamesData = filesArray.join(' ');
            return writeFilesFromArray('filenames.txt', allFilesNamesData, filesArray)
        })
        .then(([data, filesArray]) => {
            return deleteFiles(filesArray);
        })
        .then((message) => {
            console.log(message);
        })
        .catch((err) => {
            console.log(err);
        })
}

module.exports = fsCbProblem2;