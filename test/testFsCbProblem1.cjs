const path = require("path");

const mainFunction = require("../fsCbProblem1.cjs");

const pathOfDirectory = path.join(__dirname, 'chiruPro');

const numberOfFiles = 5;

mainFunction(pathOfDirectory, numberOfFiles);